
all: install

install:
	apxs2 -i -a -c -lshmht_0.1 mod_evasive.c mod_evasive_api.c mod_evasive_store_shared.c

clean:
	rm -f *.lo *.slo *.la