
#include <stdlib.h>
#include <time.h>

#ifndef MOD_EVASIVE_STORE_H
#define MOD_EVASIVE_STORE_H 1


/* ntt node (entry in the ntt root tree) */
struct ntt_node {
    char *key;
    time_t timestamp;
    long count;
    struct ntt_node *next;
};


struct ntt *ntt_create(long size);
struct ntt_node	*ntt_find(struct ntt *ntt, const char *key);
void ntt_insert(struct ntt *ntt, const char *key, time_t timestamp);
void ntt_update(struct ntt *ntt, const char * key, time_t timestamp, long count);
void ntt_cleanup(struct ntt_node ** node);
int ntt_destroy(struct ntt *ntt);
int ntt_delete(struct ntt *ntt, const char *key);

#endif
