
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <syslog.h>
#include <time.h>

#include <shmht.h>

#include "mod_evasive_store.h"


#define MEV_FILE "/tmp/mod_evasive_shared"
#define LOG( A, ... ) { \
      openlog("mod_evasive", LOG_PID, LOG_DAEMON); \
      syslog( A, __VA_ARGS__ ); \
      closelog(); \
    }

#define DEBUG
// #define DEBUG(...) LOG(LOG_ALERT, __VA_ARGS__);

/* ntt root tree */
struct ntt {
  long size;
  struct shmht * htbl;
};



/* dbj2 hash function */
unsigned int dbj2_hash(void *str_)
{
	unsigned long hash = 5381;
	char *str = (char *) str_;
	int c;

	while (c = *str++)
		hash = ((hash << 5) + hash) + c;	/* hash * 33 + c */

	return (unsigned int) hash;
}

/* comparison function */
int str_compar(void *c1, void *c2)
{
	char *c1_str = (char *) c1;
	return !bcmp (c1, c2, strlen (c1_str));
}

/* touch */

static void touch_file(char * filename) {
  char buf[1024];
  snprintf(buf, sizeof(buf), "touch %s", filename);
  system(buf);
}

/* Creates a single node in the tree */

struct ntt_node *ntt_node_create(const char *key) {
    char *node_key;
    struct ntt_node* node;

    node = (struct ntt_node *) malloc(sizeof(struct ntt_node));
    if (node == NULL) {
      return NULL;
    }
    if ((node_key = strdup(key)) == NULL) {
        free(node);
        return NULL;
    }
    node->key = node_key;
    node->timestamp = time(NULL);
    node->next = NULL;
    return(node);
}

/* Tree initializer */

struct ntt *ntt_create(long size) {
    struct ntt *ntt;
    struct shmht * htbl;
    
    ntt = (struct ntt *) malloc(sizeof(struct ntt));
    if( !ntt ) {
      return NULL;
    }
    
    // Touch file >.>
    touch_file(MEV_FILE);
    
    ntt->size = size;
    ntt->htbl = create_shmht(MEV_FILE, size, 
                              sizeof(struct ntt_node), dbj2_hash, str_compar);
    
    if( !ntt->htbl ) {
      LOG(LOG_ALERT, "Failed to create hash table");
      free(ntt);
      return NULL;
    }
    
    return ntt;
}

/* Find an object in the tree */

struct ntt_node *ntt_find(struct ntt *ntt, const char *key) {
  struct ntt_node * copy;
  void * found;
  size_t returned_size = 0;
  
  DEBUG("Start find: %s", key);
  
  // Lookup
  found = shmht_search(ntt->htbl, (void *) key, strlen(key), &returned_size);
  if( !found ) {
    DEBUG("Not found: %s", key);
    return NULL;
  }
  
  // Copy
  copy = (struct ntt_node *) malloc(sizeof(struct ntt_node));
  copy = memcpy(copy, found, sizeof(struct ntt_node));
  
  DEBUG("Found: %s %d %d", key, copy->count, copy->timestamp);
  
  return copy;
}

/* Insert a node into the tree */

void ntt_insert(struct ntt *ntt, const char *key, time_t timestamp) {
  struct ntt_node node;
  int result;
  
  DEBUG("Start insert: %s", key);
  
  // Format
  node.key = NULL;
  node.timestamp = timestamp;
  node.count = 0;
  node.next = NULL;
  
  // Remove existing
  shmht_remove(ntt->htbl, (void *) key, strlen(key));

  // Insert
  result = shmht_insert(ntt->htbl, (void *) key, strlen(key), 
                    (void *) &node, sizeof(struct ntt_node));
  
  // Insert failed
  if( result != 1 ) {
    LOG(LOG_ALERT, "Insert failed: %d", result);
  }

  DEBUG("Inserted: %s", key);
}

void ntt_update(struct ntt *ntt, const char * key, time_t timestamp, long count) {
  int result;
  struct ntt_node node;
  
  node.key = NULL;
  node.timestamp = timestamp;
  node.count = count;
  node.next = NULL;
  
  DEBUG("Start update: %s %d %d", key, count, timestamp);
  
  // Remove
  shmht_remove(ntt->htbl, (void *) key, strlen(key));
  
  // Insert
  result = shmht_insert(ntt->htbl, (void *) key, strlen(key), 
                    (void *) &node, sizeof(struct ntt_node));
  
  // Insert failed
  if( result != 1 ) {
    LOG(LOG_ALERT, "Insert failed: %d", result);
  } else {
    DEBUG("Updated: %s", key);
  }
}

void ntt_cleanup(struct ntt_node ** node)
{
  if( node ) {
    if( *node ) {
      free(*node);
    }
    *node = NULL;
  }
}

/* Tree destructor */

int ntt_destroy(struct ntt *ntt) {
    if (ntt == NULL) return -1;

    free(ntt);
    ntt = (struct ntt *) NULL;
    
    return 0;
}

/* Delete a single node in the tree */

int ntt_delete(struct ntt *ntt, const char *key) {
    shmht_remove(ntt->htbl, (void *) key, strlen(key));
    return 0;
}
