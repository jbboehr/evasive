
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <syslog.h>
#include <sys/stat.h>
#include <time.h>
#include <httpd.h>

#include "mod_evasive_api.h"
#include "mod_evasive_store.h"


unsigned long hash_table_size = DEFAULT_HASH_TBL_SIZE;
int page_count = DEFAULT_PAGE_COUNT;
int page_interval = DEFAULT_PAGE_INTERVAL;
int site_count = DEFAULT_SITE_COUNT;
int site_interval = DEFAULT_SITE_INTERVAL;
int blocking_period = DEFAULT_BLOCKING_PERIOD;
char *email_notify = NULL;
char *log_dir = NULL;
char *system_command = NULL;

int check_access(struct ntt * ntt, const char * ip, const char * uri)
{
  char hash_key[2048];
  struct ntt_node * n;
  time_t t = time(NULL);
  int ret = OK;

  /* Check whitelist */
  if( is_whitelisted(ntt, ip) ) {
    return OK;
  }

  /* First see if the IP itself is on "hold" */
  n = ntt_find(ntt, ip);

  if( n && t - n->timestamp < blocking_period ) {
    /* If the IP is on "hold", make it wait longer in 403 land */
    n->timestamp = time(NULL);
    ntt_update(ntt, ip, n->timestamp, n->count);
    ntt_cleanup(n);
    return HTTP_FORBIDDEN;
  }

  /* Not on hold, check hit stats */
  /* Has URI been hit too much? */
  snprintf(hash_key, 2048, "%s_%s", ip, uri);
  n = ntt_find(ntt, hash_key);
  if( n ) {
    /* If URI is being hit too much, add to "hold" list and 403 */
    if( t - n->timestamp < page_interval && n->count >= page_count ) {
      ret = HTTP_FORBIDDEN;
      ntt_insert(ntt, ip, time(NULL));
    } else {
      /* Reset our hit count list as necessary */
      if( t - n->timestamp >= page_interval ) {
        n->count=0;
      }
    }
    n->timestamp = t;
    n->count++;
    ntt_update(ntt, hash_key, n->timestamp, n->count);
    ntt_cleanup(n);
    return ret;
  }
  
  // Insert
  ntt_insert(ntt, hash_key, t);

  /* Has site been hit too much? */
  snprintf(hash_key, 2048, "%s_SITE", ip);
  n = ntt_find(ntt, hash_key);
  if( n ) {
    /* If site is being hit too much, add to "hold" list and 403 */
    if( t - n->timestamp < site_interval && n->count >= site_count ) {
      ret = HTTP_FORBIDDEN;
      ntt_insert(ntt, ip, time(NULL));
    } else {
      /* Reset our hit count list as necessary */
      if (t-n->timestamp>=site_interval) {
        n->count=0;
      }
    }
    n->timestamp = t;
    n->count++;
    ntt_update(ntt, hash_key, n->timestamp, n->count);
    ntt_cleanup(n);
    return ret;
  }
  
  // Insert
  ntt_insert(ntt, hash_key, t);
  
  return ret;
}

void mod_evasive_notify(const char * ip)
{
  char buffer[1024];
  struct stat s;
  FILE *file;
  
  
  // Write to file
  snprintf(buffer, sizeof(buffer), "%s/dos-%s", 
          (log_dir != NULL ? log_dir : DEFAULT_LOG_DIR), ip);
  
  if( !stat(buffer, &s) ) {
    return;
  }
  
  file = fopen(buffer, "w");
  if( !file ) {
    LOG(LOG_ALERT, "Couldn't open logfile %s: %s", buffer, strerror(errno));
    return; // meh
  }

  fprintf(file, "%ld\n", getpid());
  fclose(file);
  
  LOG(LOG_ALERT, "Blacklisting address %s: possible DoS attack.", ip);
  
  if( email_notify ) {
    snprintf(buffer, sizeof(buffer), MAILER, email_notify);
    file = popen(buffer, "w");
    if( file ) {
      fprintf(file, "To: %s\n", email_notify);
      fprintf(file, "Subject: HTTP BLACKLIST %s\n\n", ip);
      fprintf(file, "mod_evasive HTTP Blacklisted %s\n", ip);
      pclose(file);
    }
  }

  if( system_command ) {
    snprintf(buffer, sizeof(buffer), system_command, ip);
    system(buffer);
  }
}

int is_whitelisted(struct ntt * ntt, const char *ip) {
  char hashkey[128];
  char octet[4][4];
  char *dip;
  char *oct;
  int i = 0;

  memset(octet, 0, 16);
  dip = strdup(ip);
  if (dip == NULL)
    return 0;

  oct = strtok(dip, ".");
  while(oct != NULL && i<4) {
    if (strlen(oct)<=3) 
      strcpy(octet[i], oct);
    i++;
    oct = strtok(NULL, ".");
  }
  free(dip);

  /* Exact Match */
  snprintf(hashkey, sizeof(hashkey), "WHITELIST_%s", ip); 
  if (ntt_find(ntt, hashkey)!=NULL)
    return 1;

  /* IPv4 Wildcards */ 
  snprintf(hashkey, sizeof(hashkey), "WHITELIST_%s.*.*.*", octet[0]);
  if (ntt_find(ntt, hashkey)!=NULL)
    return 1;

  snprintf(hashkey, sizeof(hashkey), "WHITELIST_%s.%s.*.*", octet[0], octet[1]);
  if (ntt_find(ntt, hashkey)!=NULL)
    return 1;

  snprintf(hashkey, sizeof(hashkey), "WHITELIST_%s.%s.%s.*", octet[0], octet[1], octet[2]);
  if (ntt_find(ntt, hashkey)!=NULL)
    return 1;

  /* No match */
  return 0;
}

void whitelist(struct ntt * ntt, const char *ip)
{
  char entry[128];
  //LOG(LOG_ALERT, "Whitelisting: %s", ip);
  snprintf(entry, sizeof(entry), "WHITELIST_%s", ip);
  ntt_insert(ntt, entry, time(NULL));
}

void clearlist(struct ntt * ntt, const char *ip)
{
  char entry[128];
  //LOG(LOG_ALERT, "Clearing: %s", ip);
  snprintf(entry, sizeof(entry), "WHITELIST_%s", ip);
  ntt_delete(ntt, entry);
}
