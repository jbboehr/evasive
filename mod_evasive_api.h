
#ifndef MOD_EVASIVE_API_H
#define MOD_EVASIVE_API_H 1

#define MAILER "/bin/mail %s"
#define LOG( A, ... ) { \
      openlog("mod_evasive", LOG_PID, LOG_DAEMON);\
      syslog( A, __VA_ARGS__ );\
      closelog();\
    }

#define DEFAULT_HASH_TBL_SIZE   6151ul  // Default hash table size
#define DEFAULT_PAGE_COUNT      2       // Default maximum page hit count per interval
#define DEFAULT_SITE_COUNT      50      // Default maximum site hit count per interval
#define DEFAULT_PAGE_INTERVAL   1       // Default 1 Second page interval
#define DEFAULT_SITE_INTERVAL   1       // Default 1 Second site interval
#define DEFAULT_BLOCKING_PERIOD 10      // Default for Detected IPs; blocked for 10 seconds
#define DEFAULT_LOG_DIR         "/tmp"  // Default temp directory

struct ntt;

unsigned long hash_table_size;
int page_count;
int page_interval;
int site_count;
int site_interval;
int blocking_period;
char * email_notify;
char * log_dir;
char * system_command;

int check_access(struct ntt * ntt, const char * ip, const char * uri);
void mod_evasive_notify(const char * ip);
int is_whitelisted(struct ntt * ntt, const char * ip);
void whitelist(struct ntt * ntt, const char * ip);
void clearlist(struct ntt * ntt, const char *ip);

#endif
