/*
mod_evasive
Copyright (c) by Jonathan A. Zdziarski, modifications by John Boehr

LICENSE
                                                                                
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
                                                                                
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
                                                                                
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <errno.h>

#include "httpd.h"
#include "http_core.h"
#include "http_config.h"
#include "http_log.h"
#include "http_request.h"

#include "mod_evasive_api.h"
#include "mod_evasive_store.h"


#ifdef STANDARD20_MODULE_STUFF
#define IS_APACHE_20 1
#endif /* #ifdef STANDARD20_MODULE_STUFF */



#if IS_APACHE_20
/* Apache 2.0 */

#define CONFIG_VALUE_TYPE const char *
#define MEV_COMMAND(name, fn, desc) AP_INIT_TAKE1(name, fn, NULL, RSRC_CONF, desc)
#define MEV_INIT_API static void *
#define MEV_INIT_ARGS apr_pool_t *p, server_rec *s
#define MEV_CHILD_INIT_API static void *
#define MEV_CHILD_INIT_ARGS apr_pool_t *p, server_rec *s
#define MEV_CHILD_EXIT_API static apr_status_t 
#define MEV_CHILD_EXIT_ARGS void *not_used
#define MEV_REQ_IP(r) r->connection->remote_ip
#define mev_command_rec static const command_rec

module AP_MODULE_DECLARE_DATA evasive_module;

#else
/* Apache 1.3 */

#define CONFIG_VALUE_TYPE char *
#define MEV_COMMAND(name, fn, desc) { name, fn, NULL, RSRC_CONF, TAKE1, desc }
#define MEV_INIT_API static void
#define MEV_INIT_ARGS server_rec *s, pool *p
#define MEV_CHILD_INIT_API static void
#define MEV_CHILD_INIT_ARGS server_rec *s, pool *p
#define MEV_CHILD_EXIT_API static apr_status_t 
#define MEV_CHILD_EXIT_ARGS void *not_used
#define MEV_REQ_IP(r) inet_ntoa(r->connection->remote_addr.sin_addr)
#define mev_command_rec static command_rec

module MODULE_VAR_EXPORT evasive_module;

#endif /* #ifdef STANDARD20_MODULE_STUFF */



/* Common */
#define CONFIG_CB_ARGS cmd_parms *cmd, void *dconfig, CONFIG_VALUE_TYPE value
#define CONFIG_BODY_LONG(var, def) \
  long n = strtol(value, NULL, 0); \
  var = (n <= 0 ? def : n); \
  return NULL;
#define CONFIG_BODY_STR(var) \
  if( value && value[0] ) { \
    if( var ) { \
      free(var); \
    } \
    var = strdup(value); \
  } \
  return NULL;

struct ntt *hit_list;



/* BEGIN Hook Functions */

MEV_INIT_API mev_init(MEV_INIT_ARGS) {
  ;
}

MEV_CHILD_INIT_API mev_child_init(MEV_CHILD_INIT_ARGS) {
  hit_list = ntt_create(hash_table_size);
}

static int access_checker(request_rec * r) 
{
  int ret = OK;

  if( !r->prev && !r->main && hit_list ) {
    char * ip = MEV_REQ_IP(r);
    ret = check_access(hit_list, ip, r->uri);

    /* Perform email notification and system functions */
    if( ret == HTTP_FORBIDDEN ) {
      mod_evasive_notify(ip);
    }
  }

  if( ret == HTTP_FORBIDDEN && (ap_satisfies(r) != SATISFY_ANY || !ap_some_auth_required(r)) ) {
    ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r,
        "client denied by server configuration: %s",
        r->filename);
  }
  
  return ret;
}

MEV_CHILD_EXIT_API mev_child_exit(MEV_CHILD_EXIT_ARGS) {
  ntt_destroy(hit_list);
  free(email_notify);
  free(system_command);
}

/* END Hook Functions */



/* BEGIN Configuration Functions */

static const char * get_hash_tbl_size(CONFIG_CB_ARGS) {
  CONFIG_BODY_LONG(hash_table_size, DEFAULT_HASH_TBL_SIZE);
}

static const char * get_page_count(CONFIG_CB_ARGS) {
  CONFIG_BODY_LONG(page_count, DEFAULT_PAGE_COUNT);
}

static const char * get_site_count(CONFIG_CB_ARGS) {
  CONFIG_BODY_LONG(site_count, DEFAULT_SITE_COUNT);
}

static const char * get_page_interval(CONFIG_CB_ARGS) {
  CONFIG_BODY_LONG(page_interval, DEFAULT_PAGE_INTERVAL);
}

static const char * get_site_interval(CONFIG_CB_ARGS) {
  CONFIG_BODY_LONG(site_interval, DEFAULT_SITE_INTERVAL);
}

static const char * get_blocking_period(CONFIG_CB_ARGS) {
  CONFIG_BODY_LONG(blocking_period, DEFAULT_BLOCKING_PERIOD);
}

static const char * get_log_dir(CONFIG_CB_ARGS) {
  CONFIG_BODY_STR(log_dir);
}

static const char * get_email_notify(CONFIG_CB_ARGS) {
  CONFIG_BODY_STR(email_notify);
}

static const char * get_system_command(CONFIG_CB_ARGS) {
  CONFIG_BODY_STR(system_command);
}

static const char * get_whitelist(cmd_parms * cmd, void * dconfig, const char * ip)
{
  whitelist(hit_list, ip);
  return NULL;
}

static const char * get_clearlist(cmd_parms * cmd, void * dconfig, const char * ip)
{
  clearlist(hit_list, ip);
  return NULL;
}

/* END Configuration Functions */



mev_command_rec command_table[] = {
  MEV_COMMAND("DOSHashTableSize", get_hash_tbl_size, "Set size of hash table"),
  MEV_COMMAND("DOSPageCount", get_page_count, "Set maximum page hit count per interval"),
  MEV_COMMAND("DOSSiteCount", get_site_count, "Set maximum site hit count per interval"),
  MEV_COMMAND("DOSPageInterval", get_page_interval, "Set page interval"),
  MEV_COMMAND("DOSSiteInterval", get_site_interval, "Set site interval"),
  MEV_COMMAND("DOSBlockingPeriod", get_blocking_period, "Set blocking period for detected DoS IPs"),
  MEV_COMMAND("DOSEmailNotify", get_email_notify, "Set email notification"),
  MEV_COMMAND("DOSLogDir", get_log_dir, "Set log dir"),
  MEV_COMMAND("DOSSystemCommand", get_system_command, "Set system command on DoS"),
  MEV_COMMAND("DOSWhitelist", get_whitelist, "IP-addresses wildcards to whitelist"),
  MEV_COMMAND("DOSClearlist", get_clearlist, "IP-addresses wildcards to clear"),
  { NULL }
};



#if IS_APACHE_20
static void register_hooks(apr_pool_t *p) {
  ap_hook_access_checker(access_checker, NULL, NULL, APR_HOOK_MIDDLE);
  apr_pool_cleanup_register(p, NULL, apr_pool_cleanup_null, mev_child_exit);
};

module AP_MODULE_DECLARE_DATA evasive_module =
{
    STANDARD20_MODULE_STUFF,
    mev_init,
    NULL,
    mev_child_init,
    NULL,
    command_table,
    register_hooks
};
#else
module MODULE_VAR_EXPORT evasive_module = {
    STANDARD_MODULE_STUFF,
    mev_init,                          /* initializer */
    NULL,                              /* dir config creator */
    NULL,                              /* dir config merger */
    NULL,                              /* server config creator */
    NULL,                              /* server config merger */
    command_table,                     /* command table */
    NULL,                              /* handlers */
    NULL,                              /* filename translation */
    NULL,                              /* check_user_id */
    NULL,                              /* check auth */
    access_checker,                    /* check access */
    NULL,                              /* type_checker */
    NULL,                              /* fixups */
    NULL,                              /* logger */
    NULL,                              /* header parser */
    mev_child_init,                    /* child_init */
    mev_child_exit,                    /* child_exit */
    NULL                               /* post read-request */
};
#endif

